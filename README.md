# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Реализация нового функционала с петлей | https://yadi.sk/i/Bn_S7kkmxKqa6A |
| Предыдущий функционал | https://yadi.sk/i/GG5MvwJkuA9-gA | 
