package ru.ovechkin.tm;

import ru.ovechkin.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            command = scanner.nextLine();
            parseArg(command);
            System.out.println();
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.EXIT: exit(); break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println();
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME:\tOvechkin Roman");
        System.out.println("E-MAIL:\troman@ovechkin.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + "\t- Show developer info");
        System.out.println(TerminalConst.VERSION + "\t- Show version info");
        System.out.println(TerminalConst.HELP + "\t- Display terminal commands");
        System.out.println(TerminalConst.EXIT + "\t- Close application");
    }

}
